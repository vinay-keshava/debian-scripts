# Steps to Setup and test gitlab in a LXC Container

## Source list for bullseye-fasttrack 
```
deb http://deb.debian.org/debian bullseye main contrib
deb http://security.debian.org/debian-security bullseye-security main contrib
deb http://152.70.78.154/ fasttrack-staging main contrib
deb https://fasttrack.debian.net/debian/ bullseye-fasttrack main contrib
deb https://fasttrack.debian.net/debian/ bullseye-backports-staging main contrib
deb http://deb.debian.org/debian bullseye-backports main contrib
```

## source list for experimental 

```
deb http://deb.debian.org/debian unstable main contrib
deb https://deb.debian.org/debian experimental main contrib
deb http://152.70.78.154/ fasttrack-staging main contrib
```




## Commands to execute and setup gpg keys
```
apt install nano gpg gnupg1
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys  D3FDE3D93A1FCF50
apt install fasttrack-archive-keyring
```

## to upload breaking changing packages to private repo and rsync'ing it 
```
rsync -a repo/* ubuntu@152.70.78.154:/var/www/html
reprepro -b repo/ includedeb fasttrack-staging pkg/----the--deb-file-location
```
